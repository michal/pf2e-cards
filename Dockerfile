FROM node AS build

ARG CAPROVER_GIT_COMMIT_SHA=""
WORKDIR /app
COPY . /app
RUN npm install && \
    npm run build

FROM nginx
COPY --from=build /app/public /usr/share/nginx/html

